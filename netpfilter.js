(function () {
    const ROOT_CONTAINER_ID = "netpfilter-container";
    const LOCAL_STORAGE_KEY = "netpfilter-settings";
    
    class RestaurantList {
        constructor (params) {
            this.htmlElements = $(".shop-list-row");
            this.availableDeliveryTimeOptions = [];
            this.availableDeliveryCostOptions = [];
            this.availableMinimumOrderOptions = [];
            this.availableMinimumRatingOptions = [];
            this.availableMinimumReviewNumOptions = [];
            this.shown = 0;
            this.total = 0;
            
            this.initOptions(params);
        }

        initOptions (params) {
            let me = this;

            let uniqueDeliveryTimeValues = new Set();
            let uniqueDeliveryCostValues = new Set();
            let uniqueMinimumOrderValues = new Set();
            let uniqueMinimumRatingValues = new Set();
            let uniqueMinimumReviewNumValues = new Set();
            
            uniqueDeliveryTimeValues.add(params.delivery_time);
            uniqueDeliveryCostValues.add(params.delivery_cost);
            uniqueMinimumOrderValues.add(params.minimum_order);
            uniqueMinimumRatingValues.add(params.minimum_rating);
            uniqueMinimumReviewNumValues.add(params.minimum_review_cnt);
            
            $.each(this.htmlElements, function (index, node) {
                uniqueDeliveryTimeValues.add(me.getDeliveryTime(node));
                uniqueDeliveryCostValues.add(me.getDeliveryCost(node));
                uniqueMinimumOrderValues.add(me.getMinimumOrder(node));
                uniqueMinimumRatingValues.add(me.getRating(node));
                uniqueMinimumReviewNumValues.add(me.getReviewCount(node));
            });

            me.availableDeliveryTimeOptions = me.sortedArray(uniqueDeliveryTimeValues);
            me.availableDeliveryCostOptions = me.sortedArray(uniqueDeliveryCostValues);
            me.availableMinimumOrderOptions = me.sortedArray(uniqueMinimumOrderValues);
            me.availableMinimumRatingOptions = me.sortedArray(uniqueMinimumRatingValues).reverse();
            me.availableMinimumReviewNumOptions = me.buildRange(uniqueMinimumReviewNumValues, 50);
        }
        
        getDeliveryTime (node) {
            let jQnode = $(node);
            let delivTimeNode = jQnode.find(".shop-delivery-time");
            let textValue = this.getFirstTextNodeIn(delivTimeNode).text().trim();
            let minutes = textValue.split('-');
            return minutes[1].trim();
        }
        
        getDeliveryCost (node) {
            let jQnode = $(node);
            let delivTimeNode = jQnode.find(".shop-delivery-cost");
            let textValue = this.getFirstTextNodeIn(delivTimeNode).text().trim();
            let cost = textValue.slice(0, -3);
            return cost.replace(/\s/g, '');
        }
        
        getMinimumOrder (node) {
            let jQnode = $(node);
            let minimumOrderNode = jQnode.find(".shop-minimum-order");
            let textValue = this.getFirstTextNodeIn(minimumOrderNode).text().trim();
            let cost = textValue.slice(0, -3);
            return cost.replace(/\s/g, '');
        }
        
        getRating (node) {
            let jQnode = $(node);
            let rating = jQnode.find(".percent");
            if (rating.length > 0) {
                let textValue = $(rating).html();
                return textValue.slice(0, -1);
            } else {
                return "100";
            }
        }
        
        getReviewCount (node) {
            let jQnode = $(node);
            let parentNode = jQnode.find(".show-reviews");
            if (parentNode.length > 0) {
                let textValue = this.getTextNodesIn(parentNode).text().trim();
                let chunks = textValue.split(' ');
                let value = chunks[0].substring(2).trim();
                return value;
            } else {
                return "0";
            }
        }
        
        getClosed (node) {
            let jQnode = $(node);
            let closedNode = jQnode.find(".close");
            return closedNode.length > 0;
        }
        
        getFirstTextNodeIn (el) {
            return this.getTextNodesIn(el).first();
        }
        
        getTextNodesIn (el) {
            return $(el)
                .find(":not(iframe)")
                .addBack()
                .contents()
                .filter(function () {
                    return this.nodeType == 3;
                });
        }
        
        sortedArray (set) {
            return Array.from(set).sort(numberSortFunction);
        }
        
        buildRange (set, step) {
            let max = 0;
            
            set.forEach(function (value) {
                max = Math.max(max, Number(value));
                
            });
            
            let result = [];
            
            for (let i = 0; i*step < max; i++) {
                result.push(i*step);
            }
            
            return result;
        }
        
        applySearch (params) {
            let me = this;
            
            this.shown = 0;
            this.total = 0;

            $.each(this.htmlElements, function (index, node) {
                let elem = $(node);
                let skip = false;
                me.total++;
                
                if (!me.checkDeliveryTime(node, params.delivery_time)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip && !me.checkDeliveryCost(node, params.delivery_cost)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip && !me.checkMinimumOrder(node, params.minimum_order)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip && !me.checkRating(node, params.minimum_rating)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip && !me.checkReviewCount(node, params.minimum_review_cnt)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip && !me.checkClosed(node, params.show_closed)) {
                    elem.hide();
                    skip = true;
                }
                
                if (!skip) {
                    elem.show();
                    me.shown++;
                }
            });
            
            udpateSummary(this);
            
            if (params.save_settings) {
                storeSettings(params);
            }
        }
        
        checkDeliveryTime (node, delivery_time_limit) {
            let deliv_time = this.getDeliveryTime(node);
            
            return Number(deliv_time) <= Number(delivery_time_limit);
        }
        
        checkDeliveryCost (node, delivery_cost_limit) {
            let deliv_cost = this.getDeliveryCost(node);
            
            return Number(deliv_cost) <= Number(delivery_cost_limit);
        }
        
        checkMinimumOrder (node, minimum_order_limit) {
            let min_order = this.getMinimumOrder(node);
            
            return Number(min_order) <= Number(minimum_order_limit);
        }
        
        checkRating (node, minimum_rating_limit) {
            let rating = this.getRating(node);
            
            return Number(rating) >= Number(minimum_rating_limit);
        }
        
        checkReviewCount (node, review_count_limit) {
            let cnt = this.getReviewCount(node);
            
            return Number(cnt) >= Number(review_count_limit);
        }
        
        checkClosed (node, show_closed) {
            if (!show_closed) {
                return !this.getClosed(node);
            }
            
            return true;
        }
    }
    
    $("#"+ROOT_CONTAINER_ID).remove();
    
    let p = loadSettings();
    
    let restaurants = new RestaurantList(p);
    let shop_list_content = $(".shop-list-content");
    let root_container = $("<div>");
    root_container.addClass("netpfilter-root-container");
    root_container.attr("id", ROOT_CONTAINER_ID);
    let filter_container = $("<div>");
    filter_container.addClass("netpfilter-filter-container");
    let searchParams = {
        "delivery_time": 9999,
        "delivery_cost": 9999,
        "minimum_order": 99999,
        "minimum_rating": 0,
        "minimum_review_cnt": 0,
        "show_closed": true,
        "save_settings": false
    };
    
    if (p != null) {
        searchParams = p;
    }

    // szallitasi ido kevesebb mint
    let delivery_time_container = $("<div>");
    delivery_time_container.addClass("netpfilter-container");
    let delivery_time_label = $("<p>");
    delivery_time_label.addClass("netpfilter-label");
    delivery_time_label.html("Szállítási idő legfeljebb ");
    let delivery_time_dropdown = $("<select>");
    delivery_time_dropdown.addClass("netpfilter-select");
    delivery_time_dropdown.change(function () {
        let value = $(this).val();
        searchParams.delivery_time = value;
        restaurants.applySearch(searchParams);
    });
    appendDeliveryTimeOptions(delivery_time_dropdown);
    if (searchParams.save_settings) {
        delivery_time_dropdown.val(searchParams.delivery_time);
    }
    delivery_time_container
        .append(delivery_time_label)
        .append(delivery_time_dropdown);
    filter_container.append(delivery_time_container);

    // szallitasi koltseg kevesebb mint
    let delivery_cost_container = $("<div>");
    delivery_cost_container.addClass("netpfilter-container");
    let delivery_cost_label = $("<p>");
    delivery_cost_label.addClass("netpfilter-label");
    delivery_cost_label.html("Szállítási költség legfeljebb ");
    let delivery_cost_dropdown = $("<select>");
    delivery_cost_dropdown.addClass("netpfilter-select");
    delivery_cost_dropdown.change(function () {
        let value = $(this).val();
        searchParams.delivery_cost = value;
        restaurants.applySearch(searchParams);
    });
    appendDeliveryCostOptions(delivery_cost_dropdown);
    if (searchParams.save_settings) {
        delivery_cost_dropdown.val(searchParams.delivery_cost);
    }
    delivery_cost_container
        .append(delivery_cost_label)
        .append(delivery_cost_dropdown);
    filter_container.append(delivery_cost_container);

    // minimum rendeles kevesebb mint
    let minimum_order_container = $("<div>");
    minimum_order_container.addClass("netpfilter-container");
    let minimum_order_label = $("<p>");
    minimum_order_label.addClass("netpfilter-label");
    minimum_order_label.html("Minimum rendelés legfeljebb ");
    let minimum_order_dropdown = $("<select>");
    minimum_order_dropdown.addClass("netpfilter-select");
    minimum_order_dropdown.change(function () {
        let value = $(this).val();
        searchParams.minimum_order = value;
        restaurants.applySearch(searchParams);
    });
    appendMinimumOrderOptions(minimum_order_dropdown);
    if (searchParams.save_settings) {
        minimum_order_dropdown.val(searchParams.minimum_order);
    }
    minimum_order_container
        .append(minimum_order_label)
        .append(minimum_order_dropdown);
    filter_container.append(minimum_order_container);

    // ertekeles jobb mint
    let minimum_rating_container = $("<div>");
    minimum_rating_container.addClass("netpfilter-container");
    let minimum_rating_label = $("<p>");
    minimum_rating_label.addClass("netpfilter-label");
    minimum_rating_label.html("Értékelés legalább ");
    let minimum_rating_dropdown = $("<select>");
    minimum_rating_dropdown.addClass("netpfilter-select");
    minimum_rating_dropdown.change(function () {
        let value = $(this).val();
        searchParams.minimum_rating = value;
        restaurants.applySearch(searchParams);
    });
    appendMinimumRatingOptions(minimum_rating_dropdown);
    if (searchParams.save_settings) {
        minimum_rating_dropdown.val(searchParams.minimum_rating);
    }
    minimum_rating_container
        .append(minimum_rating_label)
        .append(minimum_rating_dropdown);
    filter_container.append(minimum_rating_container);

    // velemenyek szama nagyobb mint
    let review_count_container = $("<div>");
    review_count_container.addClass("netpfilter-container");
    let review_count_label = $("<p>");
    review_count_label.addClass("netpfilter-label");
    review_count_label.html("Vélemények száma legalább ");
    let review_count_dropdown = $("<select>");
    review_count_dropdown.addClass("netpfilter-select");
    review_count_dropdown.change(function () {
        let value = $(this).val();
        searchParams.minimum_review_cnt = value;
        restaurants.applySearch(searchParams);
    });
    appendReviewCountOptions(review_count_dropdown);
    if (searchParams.save_settings) {
        review_count_dropdown.val(searchParams.minimum_review_cnt);
    }
    review_count_container
        .append(review_count_label)
        .append(review_count_dropdown);
    filter_container.append(review_count_container);
    
    // bezart ettermek elrejtese
    let closed_filter_container = $("<div>");
    closed_filter_container.addClass("netpfilter-container");
    let closed_filter_label = $("<p>");
    closed_filter_label.addClass("netpfilter-label");
    closed_filter_label.html("Bezártak megjelenítése ");
    let closed_filter_dropdown = $("<select>");
    closed_filter_dropdown.addClass("netpfilter-select");
    closed_filter_dropdown.change(function () {
        let value = $(this).val();
        switch (value) {
            case "true":
                searchParams.show_closed = true;
                break;
            case "false":
                searchParams.show_closed = false;
                break;
            default:
                searchParams.show_closed = true;
        }
        restaurants.applySearch(searchParams);
    });
    appendClosedFilterOptions(closed_filter_dropdown);
    if (searchParams.save_settings) {
        closed_filter_dropdown.val(searchParams.show_closed ? "true" : "false");
    }
    closed_filter_container
        .append(closed_filter_label)
        .append(closed_filter_dropdown);
    filter_container.append(closed_filter_container);
    
    // etterem keresese nev alapjan

    // beallitasok megjegyzese
    let save_settings_container = $("<div>");
    save_settings_container.addClass("netpfilter-container");
    let save_settings_label = $("<p>");
    save_settings_label.addClass("netpfilter-label");
    save_settings_label.html("Szűrők mentése");
    let save_settings_dropdown = $("<select>");
    save_settings_dropdown.addClass("netpfilter-select");
    save_settings_dropdown.change(function () {
        let value = $(this).val();
        switch (value) {
            case "true":
                searchParams.save_settings = true;
                break;
            case "false":
                searchParams.save_settings = false;
                break;
            default:
                searchParams.save_settings = false;
        }
        storeSettings(searchParams);
    });
    appendSaveSettingsOptions(save_settings_dropdown);
    if (searchParams.save_settings) {
        save_settings_dropdown.val(searchParams.save_settings ? "true" : "false");
    }
    save_settings_container
        .append(save_settings_label)
        .append(save_settings_dropdown);
    filter_container.append(save_settings_container);
    
    root_container.append(filter_container);

    shop_list_content.prepend(root_container);
    
    if (searchParams.save_settings) {
        restaurants.applySearch(searchParams);
    }
    
    function udpateSummary (rest) {
        $("#netpfilter-summary").remove();
        let summary = $("<p>");
        summary.attr("id", "netpfilter-summary");
        summary.addClass("netpfilter-summary");
        summary.html("megjelenített éttermek: " + rest.shown + "/" + rest.total);
        root_container.append(summary);
    }
    
    function appendDeliveryTimeOptions (selectElement) {
        selectElement.append(emptyOption(9999));
        let values = restaurants.availableDeliveryTimeOptions;
        
        for (let i = 0; i < values.length; i++) {
            selectElement.append(valueOption(values[i], values[i] + " perc"));
        }
    }
    
    function appendDeliveryCostOptions (selectElement) {
        selectElement.append(emptyOption(9999));
        let values = restaurants.availableDeliveryCostOptions;
        
        for (let i = 0; i < values.length; i++) {
            selectElement.append(valueOption(values[i], values[i] + " Ft"));
        }
    }
    
    function appendMinimumOrderOptions (selectElement) {
        selectElement.append(emptyOption(99999));
        let values = restaurants.availableMinimumOrderOptions;
        
        for (let i = 0; i < values.length; i++) {
            selectElement.append(valueOption(values[i], values[i] + " Ft"));
        }
    }
    
    function appendMinimumRatingOptions (selectElement) {
        selectElement.append(emptyOption(0));
        let values = restaurants.availableMinimumRatingOptions;
        
        for (let i = 0; i < values.length; i++) {
            selectElement.append(valueOption(values[i], values[i] + "%"));
        }
    }
    
    function appendReviewCountOptions (selectElement) {
        selectElement.append(emptyOption(0));
        let values = restaurants.availableMinimumReviewNumOptions;
        
        for (let i = 0; i < values.length; i++) {
            selectElement.append(valueOption(values[i], values[i] + " vélemény"));
        }
    }
    
    function appendClosedFilterOptions (selectElement) {
        selectElement.append(valueOption("true", "Igen"));
        selectElement.append(valueOption("false", "Nem"));
    }
    
    function appendSaveSettingsOptions (selectElement) {
        selectElement.append(valueOption("false", "Nem"));
        selectElement.append(valueOption("true", "Igen"));
    }
    
    function emptyOption(value) {
        return valueOption(value, "");
    }
    
    function valueOption (value, label) {
        let option = $("<option>");
        option.attr("value", value);
        option.html(label);
        return option;
    }
    
    function storeSettings (params) {
        if (params.save_settings) {
            window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(params));
        } else {
            window.localStorage.removeItem(LOCAL_STORAGE_KEY);
        }
    }
    
    function loadSettings () {
         return JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEY));
    }
    
    function numberSortFunction (a, b) { return a - b; }
})();
